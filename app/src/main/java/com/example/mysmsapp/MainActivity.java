package com.example.mysmsapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements OnItemClickListener {
    private static MainActivity inst;
    ArrayList<String> smsMessagesList = new ArrayList<String>();
    ListView smsListView;
    ArrayAdapter arrayAdapter;
    private String spam;
    private String promo;

    public static MainActivity instance() {
        return inst;
    }

    @Override
    public void onStart() {
        super.onStart();
        inst = this;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        smsListView = (ListView) findViewById(R.id.SMSList);
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, smsMessagesList);
        smsListView.setAdapter(arrayAdapter);
        smsListView.setOnItemClickListener(this);

        refreshSmsInbox();
        Toast.makeText(getApplicationContext(), "msg", Toast.LENGTH_SHORT).show();
    }
    public void refreshSmsInbox() {
        ContentResolver contentResolver = getContentResolver();

        String[] projection = new String[] { "_id","date", "address", "person", "body",  "type" };
        Cursor smsInboxCursor = contentResolver.query(Uri.parse("content://sms/inbox"),
                projection, "address='TL-FEDBNK'", null,  "date desc");
        int indexBody = smsInboxCursor.getColumnIndex("body");
        int index_Id = smsInboxCursor.getColumnIndex("_id");
        int indexAddress = smsInboxCursor.getColumnIndex("address");
        if (indexBody < 0 || !smsInboxCursor.moveToFirst()) return;
        arrayAdapter.clear();
        do {
            String str = "SMS From: " + smsInboxCursor.getString(indexAddress) + "\n";
            String as = smsInboxCursor.getString(indexBody) + "\n";
            ArrayList<String> listOfR = getStr(as);
//            String std = "RS"+as.replaceAll("\\D+","_");
               // arrayAdapter.add(sx);
            arrayAdapter.addAll(as);
            arrayAdapter.add(str);
            arrayAdapter.add(listOfR);
        } while (smsInboxCursor.moveToNext());

    }

    private ArrayList<String> getStr(String message){
        String[] array = message.split(" ");
        ArrayList<String> listOfRs = new ArrayList<>();
        for(String msg :  array){
            if(msg.contains("Rs")){
                String[] princes = msg.split("Rs.");
                for(String price :  princes){
                    if(price.matches("-?\\d+(\\.\\d+)?")){
                        listOfRs.add("Rs."+price);
                    }else {
                        price = price.replace(".Call","");
                        if(price.matches("-?\\d+(\\.\\d+)?")){
                            listOfRs.add("Rs."+price);
                        }
                    }

                }

            }//end of finding the rupees
            String[] callDetails = msg.split(",");
            for(String call : callDetails){
                if(call.matches("-?\\d+(\\.\\d+)?")){
                    listOfRs.add(call);
                }
            }

        }
        Log.d("TEST",listOfRs.toString());
        return listOfRs;
    }


    public void updateList(final String smsMessage) {
        arrayAdapter.insert(smsMessage, 0);
        arrayAdapter.notifyDataSetChanged();
    }

    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
        try {
            String[] smsMessages = smsMessagesList.get(pos).split("\n");
            String address = smsMessages[0];
            String smsMessage = "";
            for (int i = 1; i < smsMessages.length; ++i) {
                smsMessage += smsMessages[i];
            }

            String smsMessageStr = address + "\n";
            smsMessageStr += smsMessage;
            Toast.makeText(this, smsMessageStr, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
